#!/usr/bin/env python

import sys
import os
import time

def id_list():
	ID = []
	os.system('scevtls -d sysop:sysop@10.110.0.132/vale_sc3 --begin "2019-06-01 00:00:00" --end "2019-07-01 00:00:00" > id.dat')
	archivo1=open("id.dat","r")
	lines=archivo1.readlines()
	for line in lines:
		ID.append(line.strip())
	for i in range (0, len(ID)):
		os.system("scevtstreams -E "+ID[i]+" -d sysop:sysop@10.110.0.132/vale_sc3 -m 600 > "+str(ID[i])+".dat")
	return

def conver_scevtstreams_fdnws(File):
	year_ini, mont_ini, day_ini, hour_ini, min_ini, sec_ini, year_end, mont_end, day_end, hour_end, min_end, sec_end, net, sta, ch = [], [], [], [], [], [], [], [], [], [], [], [], [], [], []
	contenido1 = ''
	archivo1=open(File,"r")
	ID = archivo1.name[0:11]
	lines=archivo1.readlines()
	for line in lines:
		year_ini.append(str(line[0:4]))
		mont_ini.append(str(line[5:7]))
		day_ini.append(str(line[8:10]))
		hour_ini.append(str(line[11:13]))
		min_ini.append(str(line[14:16]))
		sec_ini.append(str(line[17:19]))

		year_end.append(str(line[20:24]))
		mont_end.append(str(line[25:27]))
		day_end.append(str(line[28:30]))
		hour_end.append(str(line[31:33]))
		min_end.append(str(line[34:36]))
		sec_end.append(str(line[37:39]))
		campos1 = line.split(";")
		cont1 = 1
		cont2 = 1
		for campo1 in campos1:
			if cont1 == 3:
				cont2 = 1
				campos2 = campo1.split(".")
				for campo2 in campos2:
					if cont2 == 1:
						net.append(campo2)
					if cont2 == 2:
						sta.append(campo2)
					if cont2 == 4:
						ch.append(campo2[0:2])
					cont2 += 1
			cont1 += 1

	#print year_ini[1],mont_ini[1],day_ini[1],hour_ini[1],min_ini[1],sec_ini[1]
	#print year_end[1],mont_end[1],day_end[1],hour_end[1],min_end[1],sec_end[1]
	#print net[1],sta[1],ch[1]

	contenido1 += "quality=B\n"
	for i in range (0, len(year_ini)):
		print i
		contenido1 += net[i]+" "+sta[i]+" * "+ch[i]+"? "+year_ini[i]+"-"+mont_ini[i]+"-"+day_ini[i]+"T"+hour_ini[i]+":"+min_ini[i]+":"+sec_ini[i]+" "+year_end[i]+"-"+mont_end[i]+"-"+day_end[i]+"T"+hour_end[i]+":"+min_end[i]+":"+sec_end[i]+"\n"

	archivo2=open("req_"+str(ID)+".dat","w")
	archivo2.write(contenido1)
	archivo1.close()
	archivo2.close()
	return

def get_wave(req):
	ID = req[4:15]
	os.system("wget --post-file="+req+" -O "+ID+".mseed http://seisarc.sismo.iag.usp.br/fdsnws/dataselect/1/query??rtMax=600")
	return


id_list()
event, search = [], []
os.system("ls val*.dat > events.dat")
archivo1=open("events.dat","r")
lines=archivo1.readlines()
for line in lines:
	event.append(line.strip())
for i in range(0, len(event)):
	conver_scevtstreams_fdnws(event[i])
archivo1.close

os.system("ls req_val*.dat > search.dat")
archivo2=open("search.dat","r")
lines=archivo2.readlines()
for line in lines:
	search.append(line.strip())
for i in range(0, len(search)):
	get_wave(search[i])
archivo2.close









