#!/bin/sh
echo "insert latmin latmax" 
read latmin latmax
echo "insert lonmin lonmax" 
read lonmin lonmax
echo "insert depth"
read depth
echo "insert spacing (e.g., 0.6)"
read spacing
echo "insert read minimum number phases/ minimum pick count (e.g., 5)" 
read minphase
echo "insert diameter (e.g., 0.20 for dense local grid, 1.0 for regional grid)"
read diameter 
echo "insert max dist to station (e.g., 20 for local grid and 180 for regional grid)"
read maxdist
echo "read grid name"
read name


#-85.00 120.00  33.0  4.00 180.0 8
cat /dev/null > $name.conf

for lat in  `jot -p 1 - $latmin $latmax $spacing`
do
for lon in `jot  -p 1 - $lonmin $lonmax $spacing`
do
echo "$lat $lon $depth $diameter $maxdist $minphase" >> $name.conf
done
done
