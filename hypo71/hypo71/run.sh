#!/bin/bash
HYPO71PC_BINARY=Hypo71PC
HYPO71PC_HOME=`dirname $0`

# Jumping into the right directory
cd /opt/seiscomp3/hypo71/

# Executing binary with input file as argument
./$HYPO71PC_BINARY < input
